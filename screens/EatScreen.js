import { View, Text, SafeAreaView } from "react-native";
import React from "react";
import tw from "twrnc";
const EatScreen = () => {
  return (
    <SafeAreaView style={tw` h-full bg-blue-300`}>
      <View style={tw`p-6 `}>
        <Text>EatScreen</Text>
      </View>
    </SafeAreaView>
  );
};

export default EatScreen;
