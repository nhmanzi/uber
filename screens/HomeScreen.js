import { Image, SafeAreaView, StyleSheet, Text, View } from "react-native";
import React from "react";
import tw from "twrnc";
import NavOptions from "../components/NavOptions";

export default function HomeScreen() {
  return (
    <SafeAreaView style={tw`h-full `}>
      <View style={tw`p-4`}>
        <Image
          style={{ height: 100, width: 100, resizeMode: "contain" }}
          source={{
            uri: "https://links.papareact.com/gzs",
          }}
        />
        <NavOptions />
      </View>
    </SafeAreaView>
  );
}
