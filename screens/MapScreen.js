import { StyleSheet, Text, View, SafeAreaView } from "react-native";
import React from "react";
import tw from "twrnc";

export default function MapScreen() {
  return (
    <SafeAreaView style={tw` h-full bg-red-300`}>
      <View style={tw`p-6 `}>
        <Text>MapScreen</Text>
      </View>
    </SafeAreaView>
  );
}
