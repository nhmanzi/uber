import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";
import React from "react";
import tw from "twrnc";
import { Icon } from "react-native-elements";
import { useNavigation } from "@react-navigation/core";

const data = [
  {
    id: 1,
    title: "Get a ride",
    uri: "https://links.papareact.com/3pn",
    screen: "MapScreen",
  },
  {
    id: 2,
    title: "Get food",
    uri: "https://links.papareact.com/28w",
    screen: "EatScreen",
  },
];

const NavOptions = () => {
  const navigation = useNavigation();
  return (
    <FlatList
      style={tw`py-4`}
      data={data}
      keyExtractor={(item) => item.id}
      horizontal
      renderItem={({ item }) => (
        <TouchableOpacity onPress={() => navigation.navigate(`${item.screen}`)}>
          <View style={tw`p-4 bg-white shadow-md ml-2 rounded-xl`}>
            <Image
              style={{
                width: 120,
                height: 120,
                resizeMode: "contain",
              }}
              source={{
                uri: item.uri,
              }}
            />
            <Text style={tw`p-2`}>{item.title}</Text>
            <Icon
              style={tw`pt-2 bg-black rounded-full mt-2 w-10 h-10 `}
              name="arrowright"
              type="antdesign"
              color="white"
            />
          </View>
        </TouchableOpacity>
      )}
    />
  );
};

export default NavOptions;
